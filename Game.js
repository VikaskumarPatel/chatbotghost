const GameState = Object.freeze({
    WELCOMING:   Symbol("welcoming"),
    START:  Symbol("start"),
    RIVER: Symbol("river"),
    TRAIL: Symbol("TRAIL"),
    BRIDGE: Symbol("bridge"),
    WAIT: Symbol("wait"),
    WALK: Symbol("walk"),
    INSIDE: Symbol("inside"),
    OUTSIDE: Symbol("outside"),
    PICK: Symbol("pick"),
    HANG: Symbol("hang"),
    HOME: Symbol("home"),
});

module.exports = class Game{
    constructor(){
        this.stateCur = GameState.WELCOMING;
    }
    
    makeAMove(sInput)
    {
        let sReply = "";
        switch(this.stateCur){
            case GameState.WELCOMING:
                sReply = "Welcome to Horror Story..!! One day, you and your friend went to forest for three days campaign but because of heavy thunderstorm at night, you decided to go the nearby hotel. Is that right decision?";
                this.stateCur = GameState.START;
                break;
            case GameState.START:
                if(sInput.toLowerCase().match("yes")){
                    sReply = "You and your friend packed all things and you guys are confused about where to go. Do you want to follow river or trail?";
                    this.stateCur = GameState.RIVER;
                }else{
                    sReply ="It is a heavy thunderstorm, there is no network in your phone. There is no any chance to stay in the forest. Do you want to go the home?";
                    this.stateCur = GameState.START;
                }
                break;
            case GameState.RIVER:
                if(sInput.toLowerCase().match("river")){
                    sReply = "One of your friends know that river is crossing one bridge after 1 km, so you guys can get help from someone on the road. Pleae select bridge to go ahed.";
                    this.stateCur = GameState.BRIDGE;
                }else if(sInput.toLowerCase().match("trail")){
                    sReply = "There are so many trails around you, therefore you finally decided to follow river.";
                    this.stateCur = GameState.RIVER;
                }else{
                    sReply ="It is a heavy thunderstorm, there is no network in your phone. There is no any chance to stay in the forest. Do you want to go the home?";
                    this.stateCur = GameState.START;
                }
                break;
            case GameState.BRIDGE:
                if(sInput.toLowerCase().match("bridge")){
                    sReply = "After reaching at bridge, there are two option with you. Start walk to hotel or wait on the bridge for help.";
                    this.stateCur = GameState.WALK;

                }else if(sInput.toLowerCase().match("wait")){
                    sReply = "There is no any certainty that someone will come to help you and thunderstorm was getting worst. So, you finally decided to walk to hotel.";
                    this.stateCur = GameState.WALK;
                }
                else{
                    sReply ="It is a heavy thunderstorm, there is no network in your phone. There is no any chance to stay in the forest. Do you want to go the home?";
                    this.stateCur = GameState.START;
                }
                break;
            case GameState.WALK:
                if(sInput.toLowerCase().match("walk")){
                    sReply = "You walked around 1.5 km towards hotel and finally reached there but it looks very scary from outside. There is no light in the hotel. Do you want go inside or wait outside?";
                    this.stateCur = GameState.INSIDE;
                }else if(sInput.toLowerCase().match("outside")){
                    sReply = "There is no any certainty that someone will come to help you and thunderstorm was getting worst.";
                    this.stateCur = GameState.BRIDGE;
                }
                else{
                    sReply ="It is a heavy thunderstorm, there is no network in your phone. There is no any chance to stay in the forest. Do you want to go inside the home?";
                    this.stateCur = GameState.START;
                }
                break;
            case GameState.INSIDE:
                if(sInput.toLowerCase().match("inside")){
                    sReply = "You are afraid to go inside but you don’t have option because its heavy rain and cold wind outside. So, you went inside, it looks like there is no one in the hotel and Suddenly reception desk phone rings. Do you want to pick up the phone or hang up?";
                    this.stateCur = GameState.PICK;
                }else if(sInput.toLowerCase().match("hang")){
                    sReply = "If you hang up the phone, you have to stay at that scary hotel until next morning and that’s you don’t want because you want to reach at home safely.";
                    this.stateCur = GameState.WALK;
                }
                else{
                    sReply ="It is a heavy thunderstorm, there is no network in your phone. There is no any chance to stay in the forest. Do you want to go the home?";
                    this.stateCur = GameState.START;
                }
                break;
            case GameState.PICK:
                if(sInput.toLowerCase().match("pick")){
                    sReply = "You picked up the phone, there was manager of hotel on the call, He saw you guys from CCTV camera. There was alert system on the door also. If someone comes into hotel, an alert sends to manager automatically. He asked you, do you want to go home?";
                    this.stateCur = GameState.HOME;
                }else{
                    sReply = "It is a heavy thunderstorm, there is no network in your phone. There is no any chance to stay in the forest. Do you want to go the home?";
                    this.stateCur = GameState.START;
                }
                break;
            case GameState.HOME:
                if(sInput.toLowerCase().match("home")){
                    sReply = "Finally, you guys reached home safely, you still remember at scary night whenever you guys hangout...!! Thank you...!! ";
                    this.stateCur = GameState.HOME;
                }else{
                    sReply = "It is a heavy thunderstorm, there is no network in your phone. There is no any chance to stay in the forest. Do you want to go the home?";
                    this.stateCur = GameState.START;
                }   
                break;
        }
        return([sReply]);
    }
}