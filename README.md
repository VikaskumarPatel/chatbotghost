# Ghost Stoty Chat Bot - Halloween
---

## Developed by Vikaskumar Patel
---

## Introduction

This is a chatbot, specially design for ghost story in the Halloween season. In this chatbot, you have to send any random message to initiate the story and then after story will bring you ahed with some specific word for example "yes" or "no". The story will go ahead as per your response.
---

## Technology Listing:

* Node.js
* MongoDB
* HTML
* CSS




---

## Prerequisites

* Visual Studio Code - https://code.visualstudio.com/download
* Node.js - https://nodejs.org/en/download/
* MongoDB - https://www.mongodb.com/try/download/community

---

## Setup

### Install Dependency 

```
npm install
```

### Start Server

```
npm server
```

### Create a package.json file

```
npm init
```

### Run Application

```
node index.js
```

### Access the application on Heroku app

```
https://chatbotghost.herokuapp.com/
```


